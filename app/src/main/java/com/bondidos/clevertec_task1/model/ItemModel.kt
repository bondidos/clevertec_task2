package com.bondidos.clevertec_task1.model

data class ItemModel(
    val image: String,
    val title: String,
    val description: String
)