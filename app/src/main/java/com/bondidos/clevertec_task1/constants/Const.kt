package com.bondidos.clevertec_task1.constants

object Const {
    const val IMAGE = "image"
    const val TITLE = "title"
    const val DESCRIPTION = "description"
}